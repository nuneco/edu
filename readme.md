Currently in alpha testing.

To participate in the NunEDU program, submit a [pull request](https://opensource.stackexchange.com/a/380) to this repo.

# Programs

Our primary offering(s) to knowledge seekers is(are):  
**[Finance]()**  
Understanding the myth of money, and how automation, education, and consumption really do all balance into "enough".  
(_This is a practical guide to establishing automated income, and is an intermediate topic._)

Currently in development are curriculums on:
* interior design  
* software engineering  
* music  
* business administration  
* knitting  
* gardening
* woodworking  
* small electronics repair.

# Participating
(_A gitea.com account is currently necessary to participate, though efforts to remove that dependency are ongoing._)

Some measure of self-awareness and intersectionality is expected of participants.

Know [your trauma](https://www.dailyxtra.com/why-are-queer-people-so-mean-to-each-other-160978).

~

The [Intro to Git](https://eliotberriot.com//blog/2019/03/05/building-sotfware-together-with-git/) document is recommended reading.  
While all necessary collaboration/interaction functions are accessible via the gitea.com web interface, ingesting knowledge of the underlying version control system will help with the learning curve as you participate in the program.

=

When you are ready, [fork the repository](https://gitea.com/repo/fork/2729), commit a change, and create the pull request.

# Legal

All rights reserved, &copy; 1990-2022 NunEDU & NuneCorp.
