As you navigate this education resource with intent to learn,  
you will find notes from other knowledge seekers,  
and when you find annotation missing,

document your process

document your research

document your conclusions

Make a draft,  
and it will be refined over time.

But you must document it.

# legal

all contributions are licensed for use, reprocessing, and integration to NuneCorp, NunEDU, and all participants or representatives.
